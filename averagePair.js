/*
- create function that can accept two parameters
- first parameter will be an array of sorted integer e.g `[2,5,7,8,9,12]`
- second parameter will be a number can be integer or decimal
- your function it should determine if it's any average from summation 2 element from array in first parameter that exactly equal with the number on second parameter
- examples:
  - `averagePair([-1, 0, , 3, 4, 5, 6], 4.1)` => false
  - `averagePair([1, 2, 3], 2.5)` => true
  - `averagePair([1, 3, 3, 5, 6, 7, 10, 
*/

function averagePair(arr, num) {
  const avgList = [];
  let count = 0;

  for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < arr.length; j++) {
      if (i === j) {
        // Skip if two iterators reference the same number
        j++;
      } else {
        avgList[count] = (arr[i] + arr[j]) / 2;
        count++;
      }
    }
  }

  for (let i = 0; i < avgList.length; i++) {
    if (avgList[i] === num) {
      return true;
    }
  }

  // Set default answer
  return false;
}

console.log(averagePair([-1, 0, 3, 4, 5, 6], 4.1));
console.log(averagePair([1, 2, 3], 2.5));
console.log(averagePair([1, 3, 3, 5, 6, 7, 10, 12, 19], 8));
