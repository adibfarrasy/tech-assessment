/*
`({[]})` => true
  - `([][]{})`=> true
  - `({)(]){[}` => false
  - `[)()]` => false
*/

function isValidExpression(str) {
  let start = 0;
  let end = str.length - 1;

  while (start < end) {
    if (str[start] === "(" && str[end] !== ")") {
      return false;
    } else if (str[start] === "[" && str[end] !== "]") {
      return false;
    } else if (str[start] === "{" && str[end] !== "}") {
      return false;
    } else if (str[start] === ")" || str[start] === "]" || str[start] === "}") {
      return false;
    }
    start++;
    end--;
  }

  //Set default return value
  return true;
}

console.log(isValidExpression(`({[]})`));
console.log(isValidExpression(`({)(]){[}`));
console.log(isValidExpression(`[)()]`));
console.log(isValidExpression(`()[]{}`));
